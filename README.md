# README #

### What is it? ###

 Lightweight SCSS library
 Demo page https://fortumo-web-styles-staging.s3-eu-west-1.amazonaws.com/index.html

### Install

npm i @42mo/42mo-web-styles -D

### How to use it?

#####In SCSS:
@import '~@42mo/42mo-web-styles/lib/index';

#####How to override any library variable?
define it BEFORE importing the library, e.g 

$btn-color: blue !default;

@import '~@42mo/42mo-web-styles/lib/index';

### Developing

#### SCSS based styles
on some cases dev server restart might needed to see changes

npm run start

http://localhost:9000/dist/

#### Lit based web components

npm run start-wcs-dev-flow


### Publishing


npm publish steps:
(npm login)

npm init --scope=42mo
npm publish --access public


### Deploying

With teamcity

https://teamcity.fortumo.com/project.html?projectId=StaticWebs_WebStyles&branch_StaticWebs_WebStyles
