#!/usr/bin/env bash
STACK_NAME="WebStylesBucket"
TEMPLATE_FILE="common/public-s3-bucket/v4/bucket-stack.template.json"
STACK_POLICY_FILE="common/policies/prevent-all-instance-destructive-updates.policy.json"
CAPABILITIES="CAPABILITY_IAM"

FAMILY="PublicWebsites"
CATEGORY="Static"
COMPONENT="WebStyles"
