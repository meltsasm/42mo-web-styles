import { LitElement } from 'lit';
export declare class Expandable extends LitElement {
    static styles: import("lit").CSSResult;
    title: string;
    isOpen: boolean;
    render(): import("lit-html").TemplateResult<1>;
    private _onClick;
}
declare global {
    interface HTMLElementTagNameMap {
        'my-collapsable': Expandable;
    }
}
//# sourceMappingURL=expandable.d.ts.map