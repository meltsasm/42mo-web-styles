#!/usr/bin/env bash
STACK_NAME="WebStylesS3AccessRole"
TEMPLATE_FILE="common/public-s3-bucket/v4/public-bucket-access-role.template.json"
STACK_POLICY_FILE="common/policies/prevent-all-iamrole-destructive-updates.policy.json"
CAPABILITIES="CAPABILITY_IAM"

FAMILY="PublicWebsites"
CATEGORY="Static"
COMPONENT="WebStyles"
