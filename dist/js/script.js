

function insertAfter(referenceNode, newNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function createCodeExampleElement(innerHtml) {
  const codeExampleContainer  = document.createElement("div");
  codeExampleContainer.classList.add('code-example-container');
  const xmpEl = document.createElement("xmp");
  xmpEl.classList.add('prettyprint');
  xmpEl.classList.add('lang-html');
  const collapseToggleBtn = creatToggleButton();
  xmpEl.innerHTML = innerHtml;
  codeExampleContainer.innerHTML = collapseToggleBtn.outerHTML + xmpEl.outerHTML;
  return codeExampleContainer;
}

function creatToggleButton() {
  const showHideCodeButton = document.createElement("button");
  showHideCodeButton.innerHTML = 'Show Code &lt / &gt;';
  showHideCodeButton.classList.add('collapsible');
  return showHideCodeButton;
}

function createCopyButton(innerHtml) {
  const copyButton = document.createElement("button");
  copyButton.innerHTML = 'Copy';
  copyButton.classList.add('s-btn');
  copyButton.classList.add('copy-btn');
  const textBox = document.getElementById('clipboardText');
  copyButton.addEventListener("click", function() {
    copyTextToClipboard(innerHtml);
  });
  return copyButton;
}

function copyTextToClipboard(text) {
  const textBox = document.getElementById('clipboardText');
  textBox.value = text;
  textBox.select();
  document.execCommand("copy");
}

function initCollapsibles() {
  const coll = document.getElementsByClassName("collapsible");
  for (let i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
      this.classList.toggle("active");
      this.parentNode.classList.toggle('opened-example');
      var content = this.nextElementSibling;
      if (content.style.display === "block") {
        content.style.display = "none";
        coll[i].innerHTML = 'Show Code &lt / &gt;';
      } else {
        coll[i].innerHTML = 'Hide Code &lt / &gt;';

        content.style.display = "block";
      }
    });
  }
}

const examples = document.querySelectorAll(".code-example");

for (const el of examples) {
  const codeExampleEl = createCodeExampleElement(el.innerHTML);
  codeExampleEl.appendChild(createCopyButton(el.innerHTML));
  insertAfter(el, codeExampleEl);
}

function generateToc() {
  const titles = document.getElementsByTagName('h3');
  const toc = [];
  for (const el of titles) {
    const isMainTitle = el.classList.contains('main-title');
    if (el.id) {
      toc.push({
        elId: el.id,
        title: el.innerText,
        isMainTitle: isMainTitle,
      })
    }
  }

  const content = document.getElementById('tocContents');
  for (const link of toc) {
    if (link.isMainTitle) {
      content.innerHTML+=`<a href="#${link.elId}" class="s-link main-title my-8">${link.title}</a>`;
    } else {
      content.innerHTML+=`<a href="#${link.elId}" class="s-link ml-12 my-8">${link.title}</a>`;
    }
  }

}


initCollapsibles();

generateToc();
