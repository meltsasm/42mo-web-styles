
import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
@customElement('wc-expandable')
export class Expandable extends LitElement {
  static override styles = css`
    .collapsable-comp {
      font-family: sans-serif;
      display: flex;
      width: 100%;
      flex-direction: column;
      box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12);
      transition: min-height .3s cubic-bezier(.25,.8,.5,1);
      border-top: 1px lightgray;
    }

    .collapsible-btn {
      font-weight: bold;
      color: rgba(0,0,0,.87);
      background: #ffffff;
      width: 100%;
      box-sizing: border-box;
      height: 34px;
      display: flex;
      align-items: center;
      cursor: pointer;
      font-size: .9375rem;
      line-height: 1;
      min-height: 48px;
      outline: none;
      padding: 16px 24px;
      position: relative;
      text-align: left;
      border: none;
      border-top: 2px lightgray;
    }

    .collapsible-btn:focus {
        background: lightgray;
    }

    .content {
      font-size: 14px;
      padding: 0 8px;
      box-sizing: border-box;
      color: black;
      height: 0;
      overflow-y: hidden;
      transition: all .3s cubic-bezier(.25,.8,.5,1);
    }

    .collapsable-opened .content {
      height: auto;
      padding: 24px 8px;
    }

    .collapsable-opened .expandable-icon {
      transform: rotate(180deg);
      transition: transform .3s cubic-bezier(.25,.8,.5,1);
    }
    
    .expandable-icon {
      width: 16px;
      height: 16px;
      position: absolute;
      right: 20px;
      transform: rotate(0deg);
      transition: transform .1s linear;
    }
  `;
  @property()
  override title = '';

  @property({type: Boolean})
  isOpen = false;

  override render() {
    return html`
      <div class="collapsable-comp ${this.isOpen ? 'collapsable-opened' : 'collapsable-closed'}">
        <button type="button" class="collapsible-btn" @click=${this._onClick}>
          <img class="expandable-icon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAMAAADDpiTIAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAABcpAAAXKQE1kMoEAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAADNQTFRF////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8YBMDAAAABB0Uk5TAAIDLWKTnJ6foKGio6Tq/nVX6LcAAAYfSURBVHja7dIJchtXFARBDrXv//6nlUNeZDtICiBmgCdW5hG6+u4OAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeND9m1dGmODVm/ub9P+yvnrAhP5f15f7m/RfHjCj/7rBA37094Ah/a//gL/6e8CQ/td+wD/9PWBI/+s+YPvZ3wOG9P/jAdtN+nvAkP7Xe8D2eS0PmNd/rc/bTfp7wJD+13nAA/09YEj/azzgwf4eMKT/8Q94pL8HDOl/9AMe7e8BQ/of+4An+nvAkP5HPuDJ/h4wpP9xD/hFfw8Y0v+oB2yf1vKA36H/Wp+2m/T3gCH9j3jASf09YEj//R9wYn8PGNJ/7wec3N8DhvTf9wFn9PeAIf33fMBZ/T1gSP/9HnBmfw8Y0n+vB2wf1/KA37H/Wh+3m/T3gCH993jAs/p7wJD+lz/gmf09YEj/Sx/w7P4eMKT/ZQ+4oL8HDOl/yQMu6u8BQ/o//wEX9veAIf2f+4CL+3vAkP7Pe8D2YS0PeBn91/qw3aS/Bwzpf/4DdurvAUP6n/uA3fp7wJD+5z1gx/4eMKT/OQ/Ytb8HDOl/+gN27u8BQ/qf+oDd+3vAkP6nPeCA/h4wpP8pD9jer+UBL7X/Wu+3m/T3gCH9f/WAw/p7wJD+Tz/gwP4eMKT/Uw84tL8HDOn/+AMO7u8BQ/o/9oDD+3vAkP4PP+AK/T1gSP+HHrC9W8sDKv3XerfdpL8HDOn//wdcrb8HDOn/3wdcsb8HDOn/7wdctb8HDOn/8wFX7u8BQ/r//YCr9/eAIf3/fMAN+nvAkP4/HvB2LQ+o9l/r7d3rbx7Q7f/t9Z0HxPt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1PvnH5DvH3+A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/vEH6B9/gP4eoL8H6O8B+nuA/h6gvwfo7wH6e4D+HqC/B+jvAfp7gP4eoL8H6O8B+nuA/h6gvwfo7wH6e4D+HqC/B+jvAfp7gP4eoL8H6O8B+nuA/h6gf/wB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B//AH6AwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwEv2HYhoK2rcsbpUAAAAAElFTkSuQmCC" alt="" />
          
          ${this.title}
        </button>
        <div class="content">
          <slot></slot>
        </div>

      </div>
    `;
  }

  private _onClick() {
    this.isOpen = !this.isOpen;
    this.dispatchEvent(new CustomEvent('collapsible-toggle', {
      detail: {
        isOpen: this.isOpen,
      },
    }));
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'my-collapsable': Expandable;
  }
}
