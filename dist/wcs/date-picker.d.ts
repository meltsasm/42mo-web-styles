import { LitElement } from 'lit';
export declare class DatePicker extends LitElement {
    open: boolean;
    selectedDate: Date | null;
    selectedMonth?: number;
    selectedYear?: number;
    datePickerContainer: HTMLElement;
    inputElement: HTMLInputElement;
    static styles: import("lit").CSSResult;
    render(): import("lit-html").TemplateResult<1>;
    formatDate(date: Date | null): string;
    handleInputClick(): void;
    handleDocumentClick: (event: MouseEvent) => void;
    closeDatePicker(): void;
    getCurrentMonth(): string;
    goToPreviousMonth(): void;
    goToNextMonth(): void;
    goToPreviousYear(): void;
    goToNextYear(): void;
    renderDays(): any[];
    selectDate(date: Date): void;
}
//# sourceMappingURL=date-picker.d.ts.map