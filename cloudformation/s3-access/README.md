# Deployer S3 access roles

## Staging

```
../common/scripts/v9/create_or_update_stack.sh --action=create --parameters-file=staging/access.parameters.json \
  --stack-variables-file=variables.sh --profile=staging_superuser
```
