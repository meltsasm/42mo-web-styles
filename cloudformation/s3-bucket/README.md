# S3 bucket

## Staging

```
../common/scripts/v9/create_or_update_stack.sh --action=create --parameters-file=staging/public_bucket.parameters.json \
  --stack-variables-file=variables.sh --profile=staging_superuser
```

```
../common/scripts/v9/create_or_update_stack.sh --action=version --parameters-file=staging/public_bucket.parameters.json \
  --stack-variables-file=variables.sh --profile=staging_superuser
```
