var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { html, css, LitElement } from 'lit';
import { customElement, query, property } from 'lit/decorators.js';
let DatePicker = class DatePicker extends LitElement {
    constructor() {
        super(...arguments);
        this.open = false;
        this.selectedDate = null;
        this.handleDocumentClick = (event) => {
            const target = event.target;
            if (!target.matches('wc-date-picker')) {
                this.closeDatePicker();
            }
        };
    }
    render() {
        return html `
            <div class="date-picker-container">
                <input
                        type="text"
                        .value="${this.formatDate(this.selectedDate)}"
                        @click="${this.handleInputClick}"
                        placeholder="Select a date"
                        readonly
                />
                <div class="date-picker ${this.open ? 'open' : ''}">
                    <div class="header">

                        <div class="month-year">
                            <div class="current-month">${this.getCurrentMonth()}</div>
                        </div>
                        <div class="month-btns">
                            <button class="prev-month date-picker-day nav-btn" @click="${this.goToPreviousMonth}"
                                    title="previous month">
                                <img class="left"
                                     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAMAAADDpiTIAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAABcpAAAXKQE1kMoEAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAADNQTFRF////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8YBMDAAAABB0Uk5TAAIDLWKTnJ6foKGio6Tq/nVX6LcAAAYfSURBVHja7dIJchtXFARBDrXv//6nlUNeZDtICiBmgCdW5hG6+u4OAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeND9m1dGmODVm/ub9P+yvnrAhP5f15f7m/RfHjCj/7rBA37094Ah/a//gL/6e8CQ/td+wD/9PWBI/+s+YPvZ3wOG9P/jAdtN+nvAkP7Xe8D2eS0PmNd/rc/bTfp7wJD+13nAA/09YEj/azzgwf4eMKT/8Q94pL8HDOl/9AMe7e8BQ/of+4An+nvAkP5HPuDJ/h4wpP9xD/hFfw8Y0v+oB2yf1vKA36H/Wp+2m/T3gCH9j3jASf09YEj//R9wYn8PGNJ/7wec3N8DhvTf9wFn9PeAIf33fMBZ/T1gSP/9HnBmfw8Y0n+vB2wf1/KA37H/Wh+3m/T3gCH993jAs/p7wJD+lz/gmf09YEj/Sx/w7P4eMKT/ZQ+4oL8HDOl/yQMu6u8BQ/o//wEX9veAIf2f+4CL+3vAkP7Pe8D2YS0PeBn91/qw3aS/Bwzpf/4DdurvAUP6n/uA3fp7wJD+5z1gx/4eMKT/OQ/Ytb8HDOl/+gN27u8BQ/qf+oDd+3vAkP6nPeCA/h4wpP8pD9jer+UBL7X/Wu+3m/T3gCH9f/WAw/p7wJD+Tz/gwP4eMKT/Uw84tL8HDOn/+AMO7u8BQ/o/9oDD+3vAkP4PP+AK/T1gSP+HHrC9W8sDKv3XerfdpL8HDOn//wdcrb8HDOn/3wdcsb8HDOn/7wdctb8HDOn/8wFX7u8BQ/r//YCr9/eAIf3/fMAN+nvAkP4/HvB2LQ+o9l/r7d3rbx7Q7f/t9Z0HxPt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1PvnH5DvH3+A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/vEH6B9/gP4eoL8H6O8B+nuA/h6gvwfo7wH6e4D+HqC/B+jvAfp7gP4eoL8H6O8B+nuA/h6gvwfo7wH6e4D+HqC/B+jvAfp7gP4eoL8H6O8B+nuA/h6gf/wB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B//AH6AwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwEv2HYhoK2rcsbpUAAAAAElFTkSuQmCC"
                                     alt="">
                            </button>
                            <button class="next-month date-picker-day nav-btn" @click="${this.goToNextMonth}"
                                    title="next month">
                                <img class="right"
                                     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAMAAADDpiTIAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAABcpAAAXKQE1kMoEAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAADNQTFRF////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8YBMDAAAABB0Uk5TAAIDLWKTnJ6foKGio6Tq/nVX6LcAAAYfSURBVHja7dIJchtXFARBDrXv//6nlUNeZDtICiBmgCdW5hG6+u4OAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeND9m1dGmODVm/ub9P+yvnrAhP5f15f7m/RfHjCj/7rBA37094Ah/a//gL/6e8CQ/td+wD/9PWBI/+s+YPvZ3wOG9P/jAdtN+nvAkP7Xe8D2eS0PmNd/rc/bTfp7wJD+13nAA/09YEj/azzgwf4eMKT/8Q94pL8HDOl/9AMe7e8BQ/of+4An+nvAkP5HPuDJ/h4wpP9xD/hFfw8Y0v+oB2yf1vKA36H/Wp+2m/T3gCH9j3jASf09YEj//R9wYn8PGNJ/7wec3N8DhvTf9wFn9PeAIf33fMBZ/T1gSP/9HnBmfw8Y0n+vB2wf1/KA37H/Wh+3m/T3gCH993jAs/p7wJD+lz/gmf09YEj/Sx/w7P4eMKT/ZQ+4oL8HDOl/yQMu6u8BQ/o//wEX9veAIf2f+4CL+3vAkP7Pe8D2YS0PeBn91/qw3aS/Bwzpf/4DdurvAUP6n/uA3fp7wJD+5z1gx/4eMKT/OQ/Ytb8HDOl/+gN27u8BQ/qf+oDd+3vAkP6nPeCA/h4wpP8pD9jer+UBL7X/Wu+3m/T3gCH9f/WAw/p7wJD+Tz/gwP4eMKT/Uw84tL8HDOn/+AMO7u8BQ/o/9oDD+3vAkP4PP+AK/T1gSP+HHrC9W8sDKv3XerfdpL8HDOn//wdcrb8HDOn/3wdcsb8HDOn/7wdctb8HDOn/8wFX7u8BQ/r//YCr9/eAIf3/fMAN+nvAkP4/HvB2LQ+o9l/r7d3rbx7Q7f/t9Z0HxPt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1Pt7QL2/B9T7e0C9vwfU+3tAvb8H1PvnH5DvH3+A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/u0H6N9+gP7tB+jffoD+7Qfo336A/vEH6B9/gP4eoL8H6O8B+nuA/h6gvwfo7wH6e4D+HqC/B+jvAfp7gP4eoL8H6O8B+nuA/h6gvwfo7wH6e4D+HqC/B+jvAfp7gP4eoL8H6O8B+nuA/h6gf/wB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B/+wH6tx+gf/sB+rcfoH/7Afq3H6B//AH6AwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwEv2HYhoK2rcsbpUAAAAAElFTkSuQmCC"
                                     alt="">
                            </button>
                        </div>
                    </div>
                    <div class="weekdays">
                        <div class="weekday">S</div>
                        <div class="weekday">M</div>
                        <div class="weekday">T</div>
                        <div class="weekday">W</div>
                        <div class="weekday">T</div>
                        <div class="weekday">F</div>
                        <div class="weekday">S</div>
                    </div>

                    <div class="date-picker-content">
                        ${this.renderDays()}
                    </div>
                </div>
            </div>
        `;
    }
    formatDate(date) {
        return date ? date.toLocaleDateString() : '';
    }
    handleInputClick() {
        if (!this.open) {
            this.open = true;
            setTimeout(() => {
                this.requestUpdate();
                document.addEventListener('click', this.handleDocumentClick, { once: false });
            }, 50);
        }
    }
    closeDatePicker() {
        this.open = false;
        document.removeEventListener('click', this.handleDocumentClick);
    }
    getCurrentMonth() {
        const currentMonthYear = new Date().toLocaleString('default', { month: 'short', year: 'numeric' });
        if (this.selectedDate) {
            return this.selectedDate.toLocaleString('default', { month: 'short', year: 'numeric' });
        }
        return currentMonthYear;
    }
    goToPreviousMonth() {
        const currentDate = this.selectedDate || new Date();
        const newDate = new Date(currentDate.getFullYear(), currentDate.getMonth() - 1, 1);
        this.selectedDate = newDate;
        this.selectedMonth = newDate.getMonth();
        this.selectedYear = newDate.getFullYear();
    }
    goToNextMonth() {
        const currentDate = this.selectedDate || new Date();
        const newDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 1);
        this.selectedDate = newDate;
        this.selectedMonth = newDate.getMonth();
        this.selectedYear = newDate.getFullYear();
    }
    goToPreviousYear() {
        const currentDate = this.selectedDate || new Date();
        const newDate = new Date(currentDate.getFullYear() - 1, currentDate.getMonth(), 1);
        this.selectedDate = newDate;
        this.selectedMonth = newDate.getMonth();
        this.selectedYear = newDate.getFullYear();
    }
    goToNextYear() {
        const currentDate = this.selectedDate || new Date();
        const newDate = new Date(currentDate.getFullYear() + 1, currentDate.getMonth(), 1);
        this.selectedDate = newDate;
        this.selectedMonth = newDate.getMonth();
        this.selectedYear = newDate.getFullYear();
    }
    renderDays() {
        const today = new Date();
        const currentDate = this.selectedDate || today;
        const currentMonth = this.selectedMonth !== undefined ? this.selectedMonth : currentDate.getMonth();
        const currentYear = this.selectedYear !== undefined ? this.selectedYear : currentDate.getFullYear();
        const daysInMonth = new Date(currentYear, currentMonth + 1, 0).getDate();
        const firstDayOfMonth = new Date(currentYear, currentMonth, 1).getDay();
        const days = [];
        // Add empty cells for days before the first day of the month
        for (let i = 0; i < firstDayOfMonth; i++) {
            days.push(html `
                <div class="empty-day"></div>`);
        }
        for (let i = 1; i <= daysInMonth; i++) {
            const day = new Date(currentYear, currentMonth, i);
            const classes = [
                'date-picker-day',
                day.getMonth() !== currentMonth ? 'disabled' : '',
                day.toDateString() === (currentDate.toDateString() || '') ? 'selected' : '',
            ];
            days.push(html `
                <button class="day-btn ${classes.join(' ')}" @click="${() => this.selectDate(day)}">
                    ${i}
                </button>
            `);
        }
        return days;
    }
    selectDate(date) {
        this.selectedDate = date;
        this.closeDatePicker();
        this.dispatchEvent(new CustomEvent('date-selected', { detail: date }));
    }
};
DatePicker.styles = css `
    /* Add your custom styles here */
      
      
    :host {
      display: inline-block;
      position: relative;

      //--wc-date-picker-selected-day-bgcol: #3f51b5;
      --wc-date-picker-selected-day-col: white;
      --wc-date-picker-weekdays-col: #757575;
      --wc-date-picker-day-font-size: 11px;
      --wc-date-picker-day-font-weight: 400;
      --wc-date-picker-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.26);
      --wc-date-picker-border: 1px solid rgba(0, 0, 0, 0.12);
      --wc-date-picker-border-radius: 4px;
      --wc-date-picker-bgcol: white;
    }

    .date-picker-container {
      position: relative;
    }

    .date-picker {
      position: absolute;
      z-index: 1;
      background-color: var(--wc-date-picker-bgcol);
      border: var(--wc-date-picker-border);
      border-radius: var(--wc-date-picker-border-radius);
      box-shadow: var(--wc-date-picker-box-shadow);
      display: none;
      width: 296px;
      height: 354px;
      padding: 16px 8px;
      box-sizing: border-box;
    }

    .date-picker.open {
      display: block;
    }

    input {
      border: none;
      background: none;
      padding: 8px;
      font-size: 14px;
      border-bottom: 1px solid rgba(0, 0, 0, 0.12);
      width: 180px;
      outline: none;
    }

    .date-picker-content {
      display: grid;
      grid-template-columns: repeat(7, 1fr);
      gap: 8px;
      padding: 8px;
    }

    .date-picker-day {
      display: flex;
      align-items: center;
      justify-content: center;
      cursor: pointer;
      height: 36px;
      border-radius: 50%;
      outline: none;
      background: none;
      text-align: center;
      font-family: inherit;
      margin: 0;
      border: none;
    }

    .date-picker-day:hover,
    .date-picker-day:focus {
      background-color: #c5cae9;
    }

    .date-picker-day.selected {
      background-color: var(--wc-date-picker-selected-day-bgcol, #3f51b5);
      color: var(--wc-date-picker-selected-day-col);
    }

    .date-picker-day.disabled {
      cursor: default;
      pointer-events: none;
      color: rgba(0, 0, 0, 0.38);
    }

    .header {
      display: flex;
      width: 100%;
      justify-content: space-between;
      align-items: center;
    }
    
    .month-year {
        padding-left: 12px;
    }

    .weekdays {
        font-family: Roboto,sans-serif;
        display: flex;
        justify-content: space-between;
        color: var(--wc-date-picker-weekdays-col);
        font-size: var(--wc-date-picker-day-font-size);
        font-weight: var(--wc-date-picker-day-font-weight);
        text-align: center;
        padding: 12px 0;
        box-sizing: border-box;
        border-bottom: 1px solid rgb(117 117 117 / 26%);
    }
    
    .month-btns {
        display: flex;
    }
    
    .flex-row {
            display: flex;
            width: 100%
    }
    
    .right, .left {             
        width: 14px;
        height: 14px;
    }
    .nav-btn {
        width: 40px;
        height: 40px;
    }
    
    .right {
        transform: rotate(-90deg);
    }
    .left {
        transform: rotate(90deg);
    }

    .weekday {
      text-align: center;
      width: calc(100% / 7);
    }

    .empty-day {
      height: 36px;
      width: calc(100% / 7);
    }
    
    .day-btn {
    height: 30px;
    }
  `;
__decorate([
    property({ type: Boolean })
], DatePicker.prototype, "open", void 0);
__decorate([
    property({ type: Date })
], DatePicker.prototype, "selectedDate", void 0);
__decorate([
    property({ type: Number })
], DatePicker.prototype, "selectedMonth", void 0);
__decorate([
    property({ type: Number })
], DatePicker.prototype, "selectedYear", void 0);
__decorate([
    query('.date-picker-container')
], DatePicker.prototype, "datePickerContainer", void 0);
__decorate([
    query('input')
], DatePicker.prototype, "inputElement", void 0);
DatePicker = __decorate([
    customElement('wc-date-picker')
], DatePicker);
export { DatePicker };
//# sourceMappingURL=date-picker.js.map