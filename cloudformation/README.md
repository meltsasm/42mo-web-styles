### Autodeployment roles (deployer role and application role)

## Get common scripts from s3 into ../common/ which is ignored for git
mkdir -p common/scripts/v9/ && aws s3 sync s3://fortumo-cf-templates/common/scripts/v9/ common/scripts/v9/ --profile production_spectator && chmod +x common/scripts/v9/create_or_update_stack.sh
